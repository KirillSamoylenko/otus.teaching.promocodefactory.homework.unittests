﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }

        [Fact]
        public async void GetPrtnerLimitReturnsNotFoundResultWhenHeNotFound()
        {
            //Arrange 
            var partnerId = Guid.Parse("84374FE9-B0BE-4E3A-9E5B-56C793969C34");
            var limitId = Guid.Parse("F42B06A0-2A13-43A4-B113-DC05F098B479");
            Partner partner = null;
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                 .ReturnsAsync(partner);
            // Act
            var result = await _partnersController.GetPartnerLimitAsync(partnerId, limitId);
            var okResult = result.Result as ActionResult;

            // Assert
            okResult.Should().BeAssignableTo<NotFoundResult>().Which.StatusCode.Should().Be(404);
        }


        [Fact]
        public async void SetPartnerLimitReturnBadRequestWhenHeNotIsActive()
        {
            //Arrange 
            var partner = CreatePartner(false);
            var enddaterequest = DateTime.Parse("2022-04-01");
            var newpartnerlimit = GetLimitRequest(enddaterequest);

            List<PartnerPromoCodeLimit> partnerLimits = CreatePartnerLimits();
            partner.PartnerLimits = partnerLimits;

            _partnersRepositoryMock.Setup(m => m.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(Guid.NewGuid(), newpartnerlimit);
            // Assert
            result.Should().BeOfType<BadRequestObjectResult>().And.Match<BadRequestObjectResult>(r => r.StatusCode == 400);
        }

        [Fact]
        public async void SetPartnerLimitReturnBadRequestWhenLimitequalZero()
        {
            //Arrange 
            var enddaterequest = DateTime.Parse("2022-04-01");
            var request = GetLimitRequest(enddaterequest, 0);

            var partnerLimit = new Fixture().Build<PartnerPromoCodeLimit>().Without(p => p.CancelDate).Without(p => p.Partner).Create();
            var partnerLimits = new[] { partnerLimit };

            var partner = CreatePartner();


            Array.ForEach(partnerLimits, item =>
            {
                partner.PartnerLimits.Add(item);
            });
            _partnersRepositoryMock.Setup(m => m.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);

            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(Guid.NewGuid(), request);

            result.Should().BeOfType<BadRequestObjectResult>()
                .And.Match<BadRequestObjectResult>(x =>
                    x.StatusCode == 400 &&
                    x.Value.Equals("Лимит должен быть больше 0"));
        }


        [Fact]
        public async void SetPartnerLimit_NewZeroLimitRequest_StateIsNotChanged()
        {
            var enddaterequest = DateTime.Parse("2022-04-01");
            var request = GetLimitRequest(enddaterequest, 0);
            var partnerLimit = new Fixture().Build<PartnerPromoCodeLimit>().Without(p => p.CancelDate).Without(p => p.Partner).Create();
            var partnerLimits = new[] { partnerLimit };
            var partner = CreatePartner();

            Array.ForEach(partnerLimits, item =>
            {
                partner.PartnerLimits.Add(item);
            });

            _partnersRepositoryMock.Setup(m => m.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);

            await _partnersController.SetPartnerPromoCodeLimitAsync(Guid.NewGuid(), request);

            _partnersRepositoryMock.Verify(x => x.UpdateAsync(partner), Times.Never);
        }



        [Fact]
        public async void SetPartnerLimit_NewZeroLimitRequest_NewLimitNotAdded()
        {
            var enddaterequest = DateTime.Parse("2022-04-01");
            var request = GetLimitRequest(enddaterequest, 0);
            var partnerLimit = new Fixture().Build<PartnerPromoCodeLimit>().Without(p => p.CancelDate).Without(p => p.Partner).Create();
            var partnerLimits = new[] { partnerLimit };
            var partner = CreatePartner(true, 5);

            Array.ForEach(partnerLimits, item =>
            {
                partner.PartnerLimits.Add(item);
            });

            _partnersRepositoryMock.Setup(m => m.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);

            await _partnersController.SetPartnerPromoCodeLimitAsync(Guid.NewGuid(), request);

            partner.PartnerLimits.Count.Should().Be(partnerLimits.Length);
        }


        [Fact]
        public async void SetPartnerLimit_AddNewLimitWhenOldLimitCancelDateIsNull_OldLimitHasCancelDate()
        {
            var enddaterequest = DateTime.Parse("2022-04-01");
            var request = GetLimitRequest(enddaterequest, 0);
            var partnerLimit = new Fixture().Build<PartnerPromoCodeLimit>().Without(p => p.CancelDate)
                .Without(p => p.Partner).Create();
            var partnerLimits = new[] { partnerLimit };
            var partner = CreatePartner(true, 0);
            Array.ForEach(partnerLimits, item =>
            {
                partner.PartnerLimits.Add(item);
            });

            _partnersRepositoryMock.Setup(m => m.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);

            await _partnersController.SetPartnerPromoCodeLimitAsync(Guid.NewGuid(), request);

            partnerLimit.CancelDate.Should().NotBeNull();
        }

        [Fact]
        public async void SetPartnerLimit_AddNewLimitWhenOldLimitCancelDateIsNull_NumberIssuedPromoCodesSetZero()
        {
            var enddaterequest = DateTime.Parse("2022-04-01");
            var request = GetLimitRequest(enddaterequest, 0);
            var partnerLimit = new Fixture().Build<PartnerPromoCodeLimit>().Without(p => p.CancelDate)
                .Without(p => p.Partner).Create();
            var partnerLimits = new[] { partnerLimit };
            var partner = CreatePartner(true, 0);


            Array.ForEach(partnerLimits, item =>
            {
                partner.PartnerLimits.Add(item);
            });

            _partnersRepositoryMock.Setup(m => m.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);

            await _partnersController.SetPartnerPromoCodeLimitAsync(Guid.NewGuid(), request);

            partner.NumberIssuedPromoCodes.Should().Be(0);
        }

        [Fact]
        public async void SetPartnerLimit_AddNewLimitWhenOldLimitEndDateNotExpired_OldLimitHasNewCancelDate()
        {
            var enddaterequest = DateTime.Parse("2022-04-01");
            var request = GetLimitRequest(enddaterequest);
            var enddate = DateTime.Now.AddYears(5);
            var partnerLimit = new Fixture().Build<PartnerPromoCodeLimit>()
                .With(p => p.EndDate, enddate)
                .Without(p => p.Partner).Create();
            var partnerLimits = new[] { partnerLimit };

            var partner = CreatePartner(true, 5);

            Array.ForEach(partnerLimits, item =>
            {
                partner.PartnerLimits.Add(item);
            });


            _partnersRepositoryMock.Setup(m => m.GetByIdAsync(It.IsAny<Guid>())).ReturnsAsync(partner);

            await _partnersController.SetPartnerPromoCodeLimitAsync(Guid.NewGuid(), request);

            partnerLimit.CancelDate.Value.Day.Should().Be(DateTime.Now.Day);
        }




        public SetPartnerPromoCodeLimitRequest GetLimitRequest(DateTime setenddate, int setlimit = 1)
        {
            return new SetPartnerPromoCodeLimitRequest()
            {
                EndDate = setenddate,
                Limit = setlimit
            };
        }

        public Partner CreatePartner(bool setIsActive = true, int IssuedPromoCodes = 0)
        {

            var partner = new Partner()
            {
                Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                IsActive = setIsActive,
                NumberIssuedPromoCodes = IssuedPromoCodes,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
            };

            return partner;
        }

        public List<PartnerPromoCodeLimit> CreatePartnerLimits(int limit = 10)
        {
            return new List<PartnerPromoCodeLimit>()
            {
                new PartnerPromoCodeLimit()
                {
                    Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                    CreateDate = new DateTime(2020, 07, 9),
                    EndDate = new DateTime(2020, 10, 9),
                    Limit = limit
                }
            };
        }
    }
}
